from django.core.mail import send_mail
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.datetime_safe import datetime
from django.utils.html import strip_tags

from finalProject import settings


class Service(models.Model):
    name = models.CharField(max_length=40)
    description = models.TextField(max_length=1000)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Dentist(models.Model):
    first_name = models.CharField(max_length=40)
    last_name = models.CharField(max_length=40)
    description = models.TextField(max_length=1000)
    photography = models.ImageField(upload_to='static/img/dentists', null=True, blank=True)
    email_address = models.EmailField(max_length=100)
    phone_number = models.CharField(max_length=40)
    date_of_birth = models.DateField(default=datetime.now)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class Patient(models.Model):
    gender_choices = (('male', 'Male'), ('female', 'Female'), ('other', 'Other'))
    first_name = models.CharField(max_length=40)
    last_name = models.CharField(max_length=40)
    email_address = models.EmailField(max_length=100)
    phone_number = models.CharField(max_length=40)
    date_of_birth = models.DateField(default=datetime.now)
    dentist = models.ForeignKey(Dentist, on_delete=models.CASCADE)
    gender = models.CharField(max_length=10, choices=gender_choices)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class DentistService(models.Model):
    dentist = models.ForeignKey(Dentist, on_delete=models.CASCADE)
    service = models.ForeignKey(Service, on_delete=models.CASCADE)

    def __str__(self):
        return self.service.name


class Intervention(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=250)
    dentist = models.ForeignKey(Dentist, on_delete=models.CASCADE)
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Contact(models.Model):
    name = models.CharField(max_length=100, verbose_name="Nume")
    email = models.EmailField(verbose_name="E-mail")
    phone = models.CharField(max_length=20, verbose_name="Telefon")
    message = models.TextField(max_length=1000, verbose_name="Mesaj")
    date_created = models.DateTimeField(verbose_name="Creat la data", default=timezone.now)

    def __str__(self):
        return f'{self.email} - {self.date_created.date()}'


@receiver(post_save, sender=Contact)
def send_email_for_contact_created(sender, instance: Contact, created, **kwargs):
    if created:
        subject = 'Mesaj client Smile Story'
        msg = f'''Salut,
       <br>
       <br> Mesaj nou primit in: <b> {instance.date_created.date()} </b>, ora <b> {instance.date_created.time()} </b>.
       <br> Nume: <b> {instance.name} </b>
       <br> Email: <b> {instance.email} </b>
       <br> Telefon: <b> {instance.phone} </b>
       <br><br>
        Mesaj:
        <br>
        {instance.message}.
        '''
        plain_msg = strip_tags(msg)
        send_mail(
            subject=subject,
            message=plain_msg,
            from_email=settings.EMAIL_HOST_USER,
            recipient_list=[settings.RECIPIENT_ADDRESS],
            html_message=msg
        )


class CarouselContent(models.Model):
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=100)
    img = models.ImageField(upload_to='static/img/carousel_images')

    def __str__(self):
        return self.title


class FirstPageArticle(models.Model):
    title = models.CharField(max_length=200)
    content = models.TextField(max_length=10000)
    img = models.ImageField(upload_to='static/img/first_page_article')

    def __str__(self):
        return self.title


class FirstPageCard(models.Model):
    name = models.CharField(max_length=50)
    link_address = models.CharField(max_length=100, null=True, blank=True)
    img = models.ImageField(upload_to='static/img/first_page_article')

    def __str__(self):
        return self.name


class ProfilaxieTable(models.Model):
    name = models.CharField(max_length=80)
    price = models.CharField(max_length=15)

    def __str__(self):
        return self.name


class EndodontieTable(models.Model):
    name = models.CharField(max_length=80)
    price = models.CharField(max_length=15)

    def __str__(self):
        return self.name


class OrtodontieTable(models.Model):
    name = models.CharField(max_length=80)
    price = models.CharField(max_length=15)

    def __str__(self):
        return self.name



class OdontTerRestTable(models.Model):
    name = models.CharField(max_length=80)
    price = models.CharField(max_length=15)

    def __str__(self):
        return self.name


class ProteticaTable(models.Model):
    name = models.CharField(max_length=80)
    price = models.CharField(max_length=15)

    def __str__(self):
        return self.name


class EsteticaDentaraTable(models.Model):
    name = models.CharField(max_length=80)
    price = models.CharField(max_length=15)

    def __str__(self):
        return self.name

class PedodontieTable(models.Model):
    name = models.CharField(max_length=80)
    price = models.CharField(max_length=15)

    def __str__(self):
        return self.name