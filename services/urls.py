from django.urls import path
from services import views

urlpatterns = [
    path('', views.home_view, name='home'),
    path('all-services/', views.ServiceListView.as_view(), name='list_of_services'),
    path('all-dentists/', views.DentistListView.as_view(), name='list_of_dentists'),
    path('contact/', views.ContactCreateView.as_view(), name='contact'),
    path('price/', views.price_view, name='price'),
    path('get-all-patients-per-dentist/<int:pk>/', views.get_patients_per_dentist, name='patients_per_dentist'),
    path('get-all-interventions-per-patient/<int:pk>/', views.get_intervention_per_patient,
         name='intervention_per_patient'),

]
