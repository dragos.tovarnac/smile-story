from django.contrib.auth.decorators import login_required

from django.shortcuts import render

from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, TemplateView
from services.forms import ContactForm
from services.models import Service, Dentist, Patient, Contact, CarouselContent, FirstPageArticle, FirstPageCard, \
    ProfilaxieTable, EndodontieTable, OrtodontieTable, OdontTerRestTable, ProteticaTable, \
    Intervention, EsteticaDentaraTable, PedodontieTable


# class HomeTemplateView(TemplateView):
#     template_name = 'services/home.html'


def home_view(request):
    obj = CarouselContent.objects.all()
    first_page_article = FirstPageArticle.objects.all()
    first_page_card = FirstPageCard.objects.all()
    context = {
        'obj': obj,
        'first_page_article': first_page_article,
        'first_page_card': first_page_card,
    }
    return render(request, 'services/home.html', context)


class ServiceListView(ListView):
    template_name = 'services/list_of_services.html'
    model = Service
    context_object_name = 'all_services'

    def get_context_data(self, **kwargs):
        data = super(ServiceListView, self).get_context_data(**kwargs)
        all_services = Service.objects.all()
        data['all_services'] = all_services
        return data


class DentistListView(ListView):
    template_name = 'services/list_of_dentists.html'
    model = Dentist
    context_object_name = 'all_dentists'

    def get_context_data(self, **kwargs):
        data = super(DentistListView, self).get_context_data(**kwargs)
        all_dentists = Dentist.objects.all()
        data['all_dentists'] = all_dentists
        return data


class ContactCreateView(CreateView):
    model = Contact
    template_name = 'services/contact.html'
    form_class = ContactForm

    def get_success_url(self):
        url = reverse_lazy('contact')
        print(url)
        if self.request.method == 'POST':
            print('post')
            return url + '?success=1'
        else:
            print('get')
            return url

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['success'] = self.request.GET.get('success', 0)
        return data


class PriceView(TemplateView):
    template_name = "services/price.html"


class CarouselListView(ListView):
    template_name = 'services/home.html'
    model = CarouselContent
    context_object_name = 'carousels'

    def get_context_data(self, **kwargs):
        data = super(CarouselListView, self).get_context_data(**kwargs)
        carousels = CarouselContent.objects.all()
        data['carousels'] = carousels
        return data


def price_view(request):
    profilaxie = ProfilaxieTable.objects.all()
    endodontie = EndodontieTable.objects.all()
    ortodontie = OrtodontieTable.objects.all()
    odont_ter = OdontTerRestTable.objects.all()
    protetica = ProteticaTable.objects.all()
    estetica_dentara = EsteticaDentaraTable.objects.all()
    pedodontie = PedodontieTable.objects.all()

    context = {
        'profilaxie': profilaxie,
        'endodontie': endodontie,
        'ortodontie': ortodontie,
        'odont_ter': odont_ter,
        'protetica': protetica,
        'estetica_dentara':estetica_dentara,
        'pedodontie':pedodontie,
    }

    return render(request, 'services/price.html', context)


@login_required()
def get_patients_per_dentist(request, pk):
    get_all_patients_per_dentist = Patient.objects.filter(dentist_id=pk)
    dentist = Dentist.objects.get(id=pk)
    context = {'all_patients_per_dentist': get_all_patients_per_dentist,
               'dentist': dentist
               }
    return render(request, 'services/patients_per_dentist.html', context)


@login_required()
def get_intervention_per_patient(request, pk):
    get_all_interventions_per_patient = Intervention.objects.filter(patient_id=pk)
    patient = Patient.objects.get(id=pk)
    context = {
        'all_interventions_per_patient': get_all_interventions_per_patient,
        'patient': patient
    }
    return render(request, 'services/intervention_per_patient.html', context)
