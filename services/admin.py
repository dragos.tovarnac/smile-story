from django.contrib import admin

from services.models import Service, Dentist, Patient, Intervention, DentistService, Contact, \
    CarouselContent, FirstPageArticle, FirstPageCard, ProfilaxieTable, EndodontieTable, OrtodontieTable, \
    ProteticaTable, EsteticaDentaraTable, PedodontieTable

admin.site.register(Service)
admin.site.register(Dentist)
admin.site.register(Patient)
admin.site.register(Intervention)
admin.site.register(DentistService)
admin.site.register(Contact)
admin.site.register(CarouselContent)
admin.site.register(FirstPageArticle)
admin.site.register(FirstPageCard)
admin.site.register(ProfilaxieTable)
admin.site.register(EndodontieTable)
admin.site.register(OrtodontieTable)
admin.site.register(ProteticaTable)
admin.site.register(EsteticaDentaraTable)
admin.site.register(PedodontieTable)
