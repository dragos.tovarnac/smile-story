from django import forms
from django.forms import TextInput,  EmailInput, Textarea

from services.models import  Contact



class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ['name', 'email', 'phone', 'message']

        widgets = {
            'name': TextInput(attrs={'placeholder': 'Numele tau', 'class': 'form-control'}),
            'email': EmailInput(attrs={'placeholder': 'Adresa ta de e-mail', 'class': 'form-control'}),
            'phone': TextInput(attrs={'placeholder': 'Numarul tau de telefon', 'class': 'form-control'}),
            'message': Textarea(attrs={'placeholder': 'Mesajul tau', 'class': 'form-control', 'rows':4})
        }
