from services.models import Dentist, Patient


def get_all_dentists(request):
    dentists = Dentist.objects.all()
    return {'dentists': dentists}


def get_all_patients(request):
    patients = Patient.objects.all()
    return {'patients': patients}
